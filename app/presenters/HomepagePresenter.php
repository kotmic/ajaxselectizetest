<?php

namespace App\Presenters;

use Nette;

class HomepagePresenter extends BasePresenter
{
    /**
     * @var array
     */
    private $arrayData = [];

    public function __construct()
    {
        parent::__construct();

        $this->arrayData = array (
            array (
                'id'      => 1,
                'name'    => 'Michal Kotek',
            ),
        );
    }

    public function createComponentTestForm()
    {
        $form = new Nette\Application\UI\Form();

        $form->addSelectize('test', 'test', $this->arrayData)->setAjaxURL($this->link('ajax!'));

        return $form;
    }

    /**
     * @param string $query
     */
    public function handleAjax($query)
    {
        $this->sendJson(array_filter($this->arrayData, function ($row) use ($query) {
            return stripos($row['name'], $query) !== false;
        }));
    }
}
